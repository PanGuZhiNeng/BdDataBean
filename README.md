#### 推送给第三方的json数据，一共有三种类型的数据：回执，定位信息和通信信息。
**通信信息**中(communicationList)的**msgContent**：是16进制的字符串，需要自定义协议去解析。 

**定位信息**中(positionList)的**dtHigh**: 高程误差，单位,米 

**回执**中(receiptList)的**dataId**与**flag**:
- **dataId**:为16进制字符串，长度为24个字符。如果下发文本内容时，想要识别这条消息是否发送成功就要带上dataId
- 当下发消息时带上此**dataId**,服务器就会返回2个回执给调用方，并且这2个回执都带有**dataId**,以及一个标志位**flag**
- 第一个回执:(dataId=xxxx,**flag=1**)表示已经通过RD通道发送出去了，第二个回执：（dataId=xxxx,**flag=2**）表示对方收到了这条消息

---
```
{
	"communicationList": [{
		"destAddr": 123456,
		"msgContent": "ff",
		"sourceAddr": 654321,
		"time": 1550415406669
	}, {
		"destAddr": 123456,
		"msgContent": "ff",
		"sourceAddr": 654321,
		"time": 1550415406669
	}],
	"positionList": [{
		"dtHigh": 0,
		"high": 1000,
		"latitude": 22.33333,
		"longitude": 33.33333,
		"time": 1550415406673,
		"userAddr": 123456
	}],
	"receiptList": [{
		"dataId": "0000000a0123456789a0a0a0",
		"destAddr": 123456,
		"flag": 0,
		"sourceAddr": 654321,
		"time": 1550415406674
	}]
}
```