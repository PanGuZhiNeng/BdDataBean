package com.pancoit.push.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户接受回执数据列表，定位数据列表，通信数据列表。<br>
 * 数据中心推送数据是将用户在200ms内收到的所有数据打包、推送
 **/
public class PData {
    private List<PCommReceipt> receiptList = new ArrayList<>();
    private List<PPosition> positionList = new ArrayList<>();
    private List<PCommunication> communicationList = new ArrayList<>();

    public boolean allListEmpty() {
        if (receiptList.size() > 0 || positionList.size() > 0 || communicationList.size() > 0) {
            return false;
        }
        return true;
    }

	public List<PCommReceipt> getReceiptList() {
		return receiptList;
	}

	public void setReceiptList(List<PCommReceipt> receiptList) {
		this.receiptList = receiptList;
	}

	public List<PPosition> getPositionList() {
		return positionList;
	}

	public void setPositionList(List<PPosition> positionList) {
		this.positionList = positionList;
	}

	public List<PCommunication> getCommunicationList() {
		return communicationList;
	}

	public void setCommunicationList(List<PCommunication> communicationList) {
		this.communicationList = communicationList;
	}

}
