package com.pancoit.push.bean;

/**
 * 用户接受北斗RD定位的数据
 **/
public class PPosition {
	/**
     * 用户地址
     */
    private Long userAddr;

    /**
     * 经度
     */
    private Double longitude;

    /**
     * 纬度
     */
    private Double latitude;
    /**
     * 是否紧急，0：普通定位，1：紧急定位
     */
    private Byte emergency;
    /**
     * 高程，单位：米
     */
    private Integer high;

    /**
     * 高程误差，单位：米
     */
    private Integer dtHigh;
    /**
     * 定位时间戳 单位: 毫秒
     */
    private Long time;

	public Long getUserAddr() {
		return userAddr;
	}
	public void setUserAddr(Long userAddr) {
		this.userAddr = userAddr;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Integer getHigh() {
		return high;
	}
	public void setHigh(Integer high) {
		this.high = high;
	}
	public Integer getDtHigh() {
		return dtHigh;
	}
	public void setDtHigh(Integer dtHigh) {
		this.dtHigh = dtHigh;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	public Byte getEmergency() {
		return emergency;
	}
	public void setEmergency(Byte emergency) {
		this.emergency = emergency;
	}

}
