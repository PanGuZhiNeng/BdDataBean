package com.pancoit.push.bean;

/**
 * 用户接受通信回执信息
 **/
public class PCommReceipt {
    /**
     * 发送方地址
     */
    private Long sourceAddr;

    /**
     * 接收端地址
     */
    private Long destAddr;

    /**
     * 回执时间戳 单位：毫秒
     */
    private Long time;

    /**
     * 数据id
     */
    private String dataId;

    /**
     * 标志位，只作用于有dataId不为空情况。当flag=1,表示数据从RD通道发送出去了，当flag=2时，表示收到了回执
     */
    private Byte flag;

	public Long getSourceAddr() {
		return sourceAddr;
	}

	public void setSourceAddr(Long sourceAddr) {
		this.sourceAddr = sourceAddr;
	}

	public Long getDestAddr() {
		return destAddr;
	}

	public void setDestAddr(Long destAddr) {
		this.destAddr = destAddr;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public Byte getFlag() {
		return flag;
	}

	public void setFlag(Byte flag) {
		this.flag = flag;
	}

}
