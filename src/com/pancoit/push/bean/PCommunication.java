package com.pancoit.push.bean;

/**
 * 用户接受通信信息，通信内容透传
 **/
public class PCommunication {

    /**
     * 发送方地址
     */
    private Long sourceAddr;

    /**
     * 接收端地址
     */
    private Long destAddr;


    /**
     * 电文内容
     */
    private String msgContent;
    /**
     * 通信时间戳 单位：毫秒
     */

    private Long time;
    
	public Long getSourceAddr() {
		return sourceAddr;
	}
	public void setSourceAddr(Long sourceAddr) {
		this.sourceAddr = sourceAddr;
	}
	public Long getDestAddr() {
		return destAddr;
	}
	public void setDestAddr(Long destAddr) {
		this.destAddr = destAddr;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
}
